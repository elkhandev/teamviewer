﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace UdpTeamViewerClient.ViewModels
{
    class HostWindowViewModel : ViewModelBase
    {
        private string ip = "127.0.0.1";
        public string Ip { get => ip; set => Set(ref ip, value); }

        private int port = 8080;
        public int Port { get => port; set => Set(ref port, value); }

        private string info;
        public string Info { get => info; set => Set(ref info, value); }

        private RelayCommand startCommand;
        public RelayCommand StartCommand
        {
            get => startCommand ?? (startCommand = new RelayCommand(
                () =>
                {
                    Task.Run(() => SendImages());
                }
            ));
        }

        private void SendImages()
        {
            var udpClient = new UdpClient();
            while (true)
            {
                var bytes = CaptureScreen();
                var buffer = new byte[10000];
                var bytesCount = 0;

                var totalBytes = 0;
                var totalParts = 0;

                using (var memory = new MemoryStream(bytes))
                {
                    while ((bytesCount = memory.Read(buffer, 0, 10000)) != 0)
                    {
                        udpClient.Send(buffer, bytesCount, Ip, Port);
                        totalBytes += bytesCount;
                        totalParts++;
                    }
                }
                Info = $"Bytes: {totalBytes}; Parts {totalParts}";
                totalBytes = 0;
                totalParts = 0;
                udpClient.Send(new byte[3] { 1, 2, 3 }, 3, Ip, Port);
            }
        }

        public byte[] CaptureScreen()
        {
            var width = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width;
            var height = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Height;

            using (var memory = new MemoryStream())
            {
                Bitmap bmp = new Bitmap(width, height);
                using (Graphics g = Graphics.FromImage(bmp))
                {
                    g.CopyFromScreen(0, 0, 0, 0, System.Windows.Forms.Screen.PrimaryScreen.Bounds.Size);
                    Bitmap objBitmap = new Bitmap(bmp, (int)(width), (int)(height));
                    objBitmap.Save(memory, ImageFormat.Jpeg);
                }
                return memory.GetBuffer();
            }
        }
    }
}

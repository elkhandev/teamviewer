﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace UdpTeamViewer.ViewModels
{
    class ClientWindowViewModel : ViewModelBase
    {
        private ImageSource image;
        public ImageSource Image { get => image; set => Set(ref image, value); }

        private int port = 8080;
        public int Port { get => port; set => Set(ref port, value); }

        private string info;
        public string Info { get => info; set => Set(ref info, value); }

        private RelayCommand connectCommand;
        public RelayCommand ConnectCommand
        {
            get => connectCommand ?? (connectCommand = new RelayCommand(
                () =>
                {
                    Task.Run(() => RecieveImage());
                    //RecieveImage();
                }
            ));
        }

        public void RecieveImage()
        {
            try
            {
                var udpClient = new UdpClient(Port);
                IPEndPoint endpoint = null;

                var totalBytes = 0;
                var totalParts = 0;

                using (var memory = new MemoryStream())
                {
                    while (true)
                    {
                        var bytes = udpClient.Receive(ref endpoint);
                        if (bytes.Length == 3 && bytes[0] == 1 && bytes[1] == 2 && bytes[2] == 3)
                        {               
                            Application.Current.Dispatcher.Invoke(() =>
                            {
                                Image = ByteToImage(memory.GetBuffer());
                            });
                            memory.SetLength(0);
                            Info = $"Bytes: {totalBytes}; Parts {totalParts}";
                            totalBytes = 0;
                            totalParts = 0;
                        }
                        else
                        {
                            memory.Write(bytes, 0, bytes.Length);
                            totalBytes += bytes.Length;
                            totalParts++;
                        }
                    }
                }   
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public ImageSource ByteToImage(byte[] imageData)
        {
            try
            {
                BitmapImage biImg = new BitmapImage();
                using (MemoryStream ms = new MemoryStream(imageData))
                {
                    biImg.BeginInit();
                    biImg.StreamSource = ms;
                    biImg.EndInit();
                }
                ImageSource imgSrc = biImg as ImageSource;
                return imgSrc;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                throw;
            }
        }
    }
}
